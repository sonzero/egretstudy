class GamePresenter extends BasePresenter<GameView> implements GameListener {

    private branchs: BranchSprite[];
    private branchCollection: eui.ArrayCollection;
    //开始的那块砖头的Y坐标
    private startItemY: number;
    private marginItemTopY: number;
    private timer: egret.Timer;
    public label: string;
    public progressValue: number = 100;
    //倒数第二个开始删除
    private deleteNumber: number = 2;
    //加多一块砖头避免看到
    public static brickAdd: number = 1;
    public constructor(view: GameView) {
        super();
        this.attachView(view);
        //从屏幕外为开始坐标
        this.startItemY = -BranchSprite.HEIGHT;
        this.marginItemTopY = 0;
        this.progressValue = 100;
        //创建一个计时器对象,500ms
        this.timer = new egret.Timer(500, 0);
        //注册事件侦听器
        this.timer.addEventListener(egret.TimerEvent.TIMER, this.timerFunc, this);
        GameManager.getInstance().registerGameListener(this);
    }

    public initBranch() {
        // 用ArrayCollection包装
        if (this.branchCollection == null) {
            this.branchCollection = new eui.ArrayCollection();
            // 当数据改变的时候，ArrayCollection会派发事件
            this.branchCollection.addEventListener(eui.CollectionEvent.COLLECTION_CHANGE, this.collectionChangeHandler, this);
        } else {
            //删除所有branch
            this.removeAllBranch()
        }
        //重新填充branch数据
        for (let i: number = 0; i < SceneGame.getInstance().brickNumber; i++) {
            // console.log("i " + i + " brickNumber  " + SceneGame.getInstance().brickNumber);
            let branchSprite: BranchSprite = new BranchSprite(BranchType.DEFINE);
            // let branchSprite: BranchSprite = new BranchSprite(BranchType.DEFINE);
            branchSprite.x = SceneGame.getInstance().width / 2 - BranchSprite.WIDTH / 2;
            //从-BranchSprite.HEIGHT开始
            branchSprite.y = this.marginItemTopY + BranchSprite.HEIGHT * i;
            //回调出去添加
            this.getView().showBranchItem(branchSprite);
            //用数组记录
            // 用ArrayCollection包装
            let branchSpriteList = new eui.ArrayCollection();
            // var branchSprites: BranchSprite[] = [branchSprite];
            branchSpriteList.addItem(branchSprite);
            this.branchCollection.addItem(branchSpriteList);
        }
    }

    private createBranch() {
        //生成树木和移除树木
        this.deleteItem();
        //random随机生成左右
        var random = Math.floor(Math.random() * 3);
        // 用ArrayCollection包装
        let branchSpriteList = new eui.ArrayCollection();
        //获取最顶部的itemList
        var branchLists: eui.ArrayCollection = <eui.ArrayCollection>this.branchCollection.getItemAt(0);
        //获取当中一组最顶部的item
        var branchItem: BranchSprite = <BranchSprite>branchLists.getItemAt(0);
        if (branchItem.type != BranchType.DEFINE) {
            let branchSprite: BranchSprite = new BranchSprite(BranchType.DEFINE);
            branchSprite.setType(BranchType.DEFINE);
            branchSprite.x = SceneGame.getInstance().width / 2 - BranchSprite.WIDTH / 2;
            branchSprite.y = this.marginItemTopY;
            branchSpriteList.addItem(branchSprite);
        } else {
            if (random % 3 == 0) {
                for (let i: number = 0; i < 3; i++) {
                    let branchSprite: BranchSprite = new BranchSprite(BranchType.LEFT);
                    branchSprite.setType(BranchType.LEFT);
                    branchSprite.x = SceneGame.getInstance().width / 2 - BranchSprite.WIDTH / 2 - BranchSprite.WIDTH * i;
                    branchSprite.y = this.marginItemTopY;
                    branchSpriteList.addItem(branchSprite);
                }
            } else if (random % 3 == 1) {
                for (let i: number = 0; i < 3; i++) {
                    let branchSprite: BranchSprite = new BranchSprite(BranchType.RIGHT);
                    branchSprite.setType(BranchType.RIGHT);
                    branchSprite.x = SceneGame.getInstance().width / 2 - BranchSprite.WIDTH / 2 + BranchSprite.WIDTH * i;
                    branchSprite.y = this.marginItemTopY;
                    branchSpriteList.addItem(branchSprite);
                }
            } else {
                let branchSprite: BranchSprite = new BranchSprite(BranchType.DEFINE);
                branchSprite.setType(BranchType.DEFINE);
                branchSprite.x = SceneGame.getInstance().width / 2 - BranchSprite.WIDTH / 2;
                branchSprite.y = this.marginItemTopY;
                branchSpriteList.addItem(branchSprite);
            }
        }
        for (let i: number = 0; i < branchSpriteList.length; i++) {
            this.getView().showBranchItem(branchSpriteList.getItemAt(i));
        }
        this.branchCollection.addItemAt(branchSpriteList, 0);
    }

    public updateView(evt: egret.TouchEvent) {
        if (!GameManager.isAlive) return;
        //判断是不是一半
        let direction = SceneGame.getInstance().width / 2 - evt.stageX > 0 ? BranchType.LEFT : BranchType.RIGHT;
        // console.log("isHalf " + direction + " evt.stageX  " + evt.stageX);
        //根据位置显示英雄
        this.getView().showPlayer(direction);
        this.createBranch();
        //获取倒数第二部的itemList
        var branchLists: eui.ArrayCollection = <eui.ArrayCollection>this.branchCollection.getItemAt(this.branchCollection.length - this.deleteNumber);
        //获取倒数第二顶部的item
        var branchItem: BranchSprite = <BranchSprite>branchLists.getItemAt(0);
        if (GameData.player.type == branchItem.type) {
            GameManager.isAlive = false;
        } else {
            GameManager.isAlive = true;
        }
        //更新分数状态
        GameManager.getInstance().updateScore();
        this.updateLabel();
        if (GameManager.isAlive) {
            SoundManager.getInstance().playRight();
            //增加时间值
            this.addTime();
        } else {
            GameManager.getInstance().stopGame();
        }
        this.getView().updateView();
    }

    private deleteItem() {
        for (let i: number = 0; i < this.branchCollection.length - 1; i++) {
            if (i == this.branchCollection.length - this.deleteNumber) {
                var branchItemList: eui.ArrayCollection = this.branchCollection.getItemAt(i) as eui.ArrayCollection;
                for (let j: number = 0; j < branchItemList.length; j++) {
                    var branchItem: BranchSprite = <BranchSprite>branchItemList.getItemAt(j);
                    this.getView().startShowCartoon(branchItem);
                }
            } else if (i != this.branchCollection.length - 1) {
                var branchItemList: eui.ArrayCollection = this.branchCollection.getItemAt(i) as eui.ArrayCollection;
                for (let j: number = 0; j < branchItemList.length; j++) {
                    var branchItem: BranchSprite = <BranchSprite>branchItemList.getItemAt(j);
                    //初始化其他砖块的高度，否则因为动画做不过来就会出现其他砖块重叠的情况,由于marginItemTopY加多了一块砖头。所以这里i要加多1
                    branchItem.y = this.marginItemTopY + BranchSprite.HEIGHT * (i + 1);
                    this.getView().startMoveCartoon(branchItem);
                }
            }
        }
        this.branchCollection.removeItemAt(this.branchCollection.length - this.deleteNumber);
    }

    private collectionChangeHandler(evt: eui.CollectionEvent): void {
        // console.log("数据已改变:" + evt.kind + "," + evt.target.length + " " + this.branchCollection);
        if (evt.kind == eui.CollectionEvent.REMOVED) {
            // evt.target.isRemove = true;
        };
    }

    private timerFunc() {
        this.loseTime();
        this.getView().updateView();
    }

    private addTime() {
        if (this.progressValue < 101) {
            this.progressValue = this.progressValue + 1;
        }
    }

    private loseTime() {
        this.progressValue = this.progressValue - 1;
    }

    public startGame() {
        this.updateLabel();
        this.getView().startGame();
        this.timer.start();
    }

    public restartGame() {
        this.updateLabel();
        this.initBranch();
        this.timer.start();
    }

    public stopGame(isBackHome: boolean) {
        if (isBackHome) {
            this.getView().backHome();
        } else {
            this.getView().addDeathGame();
        }
        this.timer.stop();
    }

    private updateLabel() {
        if (GameManager.isAlive) {
            this.label = "分数: " + GameManager.getInstance().score;
        } else {
            this.label = "死亡: ";
        }
    }

    //删除所有branch
    private removeAllBranch() {
        for (let i: number = 0; i < this.branchCollection.length; i++) {
            var branchItemList: eui.ArrayCollection = this.branchCollection.getItemAt(i) as eui.ArrayCollection;
            for (let j: number = 0; j < branchItemList.length; j++) {
                var branchItem: BranchSprite = <BranchSprite>branchItemList.getItemAt(j);
                branchItem.destory();
            }
        }
        this.branchCollection.removeAll();
    }

    /**
     * 根据百分比来获取距离
     */
    public setStartY(startPercent: number) {
        this.marginItemTopY = -BranchSprite.HEIGHT * (startPercent + GamePresenter.brickAdd);
        // console.log("setStartY: " + this.marginItemTopY + " startPercent " + startPercent);

    }



}