interface GameView extends BaseView {


    updateView();
    //下移动画
    startMoveCartoon(branchItem: BranchSprite);
    //砖块飞出去动画
    startShowCartoon(branchItem: BranchSprite);
    //显示砖块
    showBranchItem(branchSprite: BranchSprite);

    addBeginGame();

    addDeathGame();

    showPlayer(direction: BranchType);

    startGame();

    backHome();

}