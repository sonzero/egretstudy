class SceneGame extends BaseComponent<GamePresenter, GameView>{

	private mPlayer: Player;
	public bg_img: eui.Image;
	public base_tree: eui.Image;
	public score_text: eui.Label;
	private static mInstance: SceneGame;
	public land_img: eui.Image;
	private brickWidth: number;
	//需要用到的砖块个数
	public brickNumber: number;
	//开始的那块砖头的Y坐标
	private startItemY: number;
	//时间进度条
	public time_bar: eui.ProgressBar;
	private gamePresenter: GamePresenter;
	public static getInstance() {
		if (!SceneGame.mInstance) {
			SceneGame.mInstance = new SceneGame();
		}
		return SceneGame.mInstance;
	}

	public constructor() {
		super();
		Utils.sceenAuto(this);
	}

	public createPresenter(): GamePresenter {
		this.gamePresenter = new GamePresenter(this);
		return this.gamePresenter;
	};

	public createView(): GameView {
		return this;
	};

	//获取舞台高度和宽度
	get width() {
		return this.parent.width;
	}
	get height() {
		return this.parent.height;
	}

	private initView() {
		this.time_bar.visible = false;
		this.score_text.visible = false;
		//隐藏文字
		this.time_bar.labelDisplay.visible = false;
		let parent: egret.DisplayObjectContainer = this;
		//创建主角,后面做成单列
		if (GameData.player == null) {
			GameData.player = new Player();
		}
		//把位置适配好
		this.brickWidth = 100;
		//从屏幕外为开始坐标
		this.startItemY = BranchSprite.HEIGHT / 2;
		//100是先补,后面要修改role图
		GameData.player.x = this.width / 2 - GameData.player.width - 50;
		parent.addChild(GameData.player);
		//计算原本需要多少个砖块
		var resultNumber = ((this.height - this.land_img.height + this.startItemY) / BranchSprite.HEIGHT);
		//获取小数点后面两位计算percent
		var pointNumber = GameUtils.getNumberPoint(resultNumber, 2);
		var startPercent = 0;
		//超过0.5块,就通过四舍五入+1块砖头，否则就自己添加一块砖头上去。
		if (pointNumber < 0.5) {
			this.brickNumber = Math.round(resultNumber + GamePresenter.brickAdd + 1);
		} else {
			this.brickNumber = Math.round(resultNumber + GamePresenter.brickAdd);
		}
		var startPercent = startPercent = 1 - pointNumber;
		this.gamePresenter.setStartY(startPercent);
		GameData.player.y = (this.height - this.land_img.height) - GameData.player.height + 15;
		//点击事件
		this.addEventListener(egret.TouchEvent.TOUCH_END, this.onClickView, this);

		this.gamePresenter.initBranch();
		//初始化
		GameManager.getInstance();
		this.addBeginGame();
		SoundManager.getInstance().playBgMusic();
	}

	//更新分数和进度条信息
	public updateView() {
		this.time_bar.value = this.gamePresenter.progressValue;
		this.score_text.text = this.gamePresenter.label;
		//设置置顶
		this.addChildAt(this.time_bar, this.numChildren - 1);
		this.addChildAt(this.score_text, this.numChildren - 1);
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
		//初始化动态增加人,相当于oncreateView
		// this.addEventListener(egret.Event.ADDED_TO_STAGE, this.initView, this);
		this.initView();
	}

	private onClickView(evt: egret.TouchEvent) {
		this.gamePresenter.updateView(evt);
	}

	/**
	 * 根据当前位置显示player的位置，并且做出对应的动画操作
	 */
	public showPlayer(direction: BranchType) {
		GameData.player.setType(direction);
		if (direction == BranchType.LEFT) {
			GameData.player.x = this.parent.width / 2 - GameData.player.leftWidth - 50;
		} else if (direction == BranchType.RIGHT) {
			GameData.player.x = this.parent.width / 2;
		}
	}

	//下移动画
	public startMoveCartoon(branchItem: BranchSprite) {
		branchItem.startMoveCartoon();
	}
	//砖块飞出去动画
	public startShowCartoon(branchItem: BranchSprite) {
		branchItem.startShowCartoon(GameData.player.type);
	}

	public showBranchItem(branchSprite: BranchSprite) {
		this.addChild(branchSprite);
	}

	public addBeginGame() {
		//添加游戏主界面
		this.parent.addChild(SceneBlack.getInstance());
		this.parent.addChild(SceneBegin.getInstance());
	}

	public addDeathGame() {
		//添加death主界面
		this.parent.addChild(SceneBlack.getInstance());
		this.parent.addChild(SceneDeath.getInstance());
		SceneDeath.getInstance().updateView();
	}

	public startGame() {
		this.time_bar.visible = true;
		this.score_text.visible = true;
	}

	public backHome() {
		this.initView();
	}


}