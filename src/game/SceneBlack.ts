class SceneBlack extends egret.Sprite {

	private _rankMask: egret.Shape;
	private static mInstance: SceneBlack;

	public static getInstance() {
		if (!SceneBlack.mInstance) {
			SceneBlack.mInstance = new SceneBlack();
		}
		return SceneBlack.mInstance;
	}

	public constructor() {
		super();
		this.init();
	}

	private init() {
		this._rankMask = new egret.Shape();
		this._rankMask.graphics.beginFill(0x000000, 0.5);
		this._rankMask.graphics.drawRect(0, 0, Config.StageWidth, Config.StageHeight);
		this._rankMask.graphics.endFill();
		this.addChild(this._rankMask);
		this._rankMask.touchEnabled = true;
		this._rankMask.visible = true;
	}



}