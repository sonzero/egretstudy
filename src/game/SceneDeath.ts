class SceneDeath extends eui.Component implements eui.UIComponent {

	private static mInstance: SceneDeath;
	private blackBmp: egret.Bitmap;
	// public btn_video: eui.Button;
	public btn_again: eui.Button;
	public btn_share: eui.Button;
	public label_score: eui.Label;
	public btn_close: eui.Button;


	public static getInstance() {
		if (!SceneDeath.mInstance) {
			SceneDeath.mInstance = new SceneDeath();
		}
		return SceneDeath.mInstance;
	}

	public constructor() {
		super();
		this.skinName = "resource/game_eui/SceneDeath.exml";
	}

	//添加皮肤的时候自动调用该函数
	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}

	// 组件加载完毕之后调用该函数,相当于是oncreate
	protected childrenCreated(): void {
		super.childrenCreated();
		this.init();
	}

	private init() {
		this.btn_again.addEventListener(egret.TouchEvent.TOUCH_TAP, this.beginGame, this);
		this.btn_share.addEventListener(egret.TouchEvent.TOUCH_TAP, this.restartGame, this);
		// this.btn_video.addEventListener(egret.TouchEvent.TOUCH_TAP, this.restartGame, this);
		this.btn_close.addEventListener(egret.TouchEvent.TOUCH_TAP, this.stopGame, this);
	}

	public updateView() {
		this.label_score.text = String(GameManager.getInstance().getScore());
		console.log("SceneDeath - init ");
		WeixinUtils.saveWxScore(this.label_score.text);
	}

	private beginGame() {
		//移除场景
		let parent: egret.DisplayObjectContainer = this.parent;
		parent.removeChild(this);
		parent.removeChild(SceneBlack.getInstance());
		GameManager.getInstance().restartGame(false);
		//添加游戏开始场景
		// parent.addChild(SceneGame.getInstance());
	}

	private restartGame() {
		WeixinUtils.shareWx();
		//移除场景
		let parent: egret.DisplayObjectContainer = this.parent;
		parent.removeChild(this);
		parent.removeChild(SceneBlack.getInstance());
		GameManager.getInstance().restartGame(true);
		//添加游戏开始场景
		// parent.addChild(SceneGame.getInstance());
	}

	private stopGame() {
		//移除场景
		let parent: egret.DisplayObjectContainer = this.parent;
		parent.removeChild(this);
		parent.removeChild(SceneBlack.getInstance());
		GameManager.getInstance().backHome();
		//添加游戏开始场景
	}

}