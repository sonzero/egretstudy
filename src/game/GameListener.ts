interface GameListener extends BaseView {


    startGame();

    stopGame(isBackHome: boolean);

    restartGame();

}