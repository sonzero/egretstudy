class SceneBegin extends eui.Component implements eui.UIComponent {

	public btn_begin: eui.Button;
	private static mInstance: SceneBegin;
	// public bth_music: eui.Button;
	public bth_rank: eui.Button;
	public bth_close: egret.Bitmap;
	private blackBmp: egret.Bitmap;
	private rankBitmap: egret.Bitmap;
	public isdisplay: boolean = false;
	private rankingListMask: egret.Shape;
	public bth_music: eui.ToggleButton;
	public bth_share: eui.Button;

	public static getInstance() {
		if (!SceneBegin.mInstance) {
			SceneBegin.mInstance = new SceneBegin();
		}
		return SceneBegin.mInstance;
	}

	public constructor() {
		super();
		this.touchChildren = true;
	}

	//添加皮肤的时候自动调用该函数
	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}

	// 组件加载完毕之后调用该函数,相当于是oncreate
	protected childrenCreated(): void {
		super.childrenCreated();
		this.init();
	}

	private init() {
		SoundManager.getInstance();
		this.btn_begin.addEventListener(egret.TouchEvent.TOUCH_TAP, this.beginGame, this);
		this.bth_rank.addEventListener(egret.TouchEvent.TOUCH_TAP, this.rankShow, this);
		this.bth_share.addEventListener(egret.TouchEvent.TOUCH_TAP, this.shareGame, this);
		//初始化时候要设置按钮状态
		this.bth_music.addEventListener(eui.UIEvent.CHANGE, this.setEffect, this);

		this.bth_music.selected = !SoundManager.getInstance().isEffect;
	}

	private beginGame() {
		//移除场景
		console.log("risheng-----beginGame");
		let parent: egret.DisplayObjectContainer = this.parent;
		parent.removeChild(this);
		parent.removeChild(SceneBlack.getInstance());
		GameManager.getInstance().startGame();
		//添加游戏开始场景
		// parent.addChild(SceneGame.getInstance());
	}

	private rankShow() {
		SoundManager.getInstance().playClick();
		let plathform: any = window.platform;
		//测试
		console.log("测试score");
		if (!this.isdisplay) {
			this.isdisplay = true;
			// this.addChild(SceneWxRank.getInstance());
			//处理遮罩,避免开放域数据影响主域
			this.rankingListMask = new egret.Shape();
			this.rankingListMask.graphics.beginFill(0x000000, 1);
			this.rankingListMask.graphics.drawRect(0, 0, this.stage.width, this.stage.height);
			this.rankingListMask.graphics.endFill();
			this.rankingListMask.alpha = 0.4;
			//设置为true,以免触摸到下面的按钮
			// this.rankingListMask.touchEnabled = true;
			// this.rankingListMask.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onMask, this);
			this.addChild(this.rankingListMask);
			//显示开放域数据
			this.rankBitmap = plathform.openDataContext.createDisplayObject(null, this.stage.stageWidth, this.stage.stageHeight);
			// this.rankBitmap.touchEnabled = true;
			this.addChild(this.rankBitmap);
			//主域向子域发送数据
			plathform.openDataContext.postMessage({
				isRanking: this.isdisplay,
				type: "friend",
				text: "egret",
				year: (new Date()).getFullYear(),
				command: "open"
			});
			this.bth_close = GameUtils.createBitmapByName("ic_close_png");
			this.bth_close.x = 520;
			this.bth_close.y = 80;
			this.bth_close.touchEnabled = true;
			//简单实现，打开这关闭使用一个按钮。
			this.bth_close.addEventListener(egret.TouchEvent.TOUCH_TAP, this.rankShow, this);
			this.addChild(this.bth_close);
		} else {
			console.log("close测试score");
			this.rankBitmap.parent && this.rankBitmap.parent.removeChild(this.rankBitmap);
			this.rankingListMask.parent && this.rankingListMask.parent.removeChild(this.rankingListMask);
			this.removeChild(this.bth_close);
			this.isdisplay = false;
			plathform.openDataContext.postMessage({
				isRanking: this.isdisplay,
				text: "egret",
				year: (new Date()).getFullYear(),
				command: "close"
			});
		}
	}

	private setEffect(evt: eui.UIEvent) {
		if (SoundManager.getInstance().isEffect) {
			SoundManager.getInstance().isEffect = false;
			evt.target.selected = true;
		} else {
			SoundManager.getInstance().isEffect = true;
			evt.target.selected = false;
		}
		//关闭bgm
		if (SoundManager.getInstance().isMusic) {
			SoundManager.getInstance().isMusic = false;
		} else {
			SoundManager.getInstance().isMusic = true;
		}

	}

	private shareGame() {
		WeixinUtils.shareWx();
		
	}

}