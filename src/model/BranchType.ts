/**
 * 树枝类型
 * 
 */
enum BranchType {
	DEFINE,//无节点
	LEFT,//左
	RIGHT,//右
	BASE,//树根
}