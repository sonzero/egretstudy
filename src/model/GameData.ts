class GameData {

	/**
 * 伐木工游戏角色
 */
	static player: Player
	/**
	 * 游戏是否开始
	 */
	static hasStart: boolean;
	/**
	 * 对象是否存活
	 */
	static isAlive: boolean;
	/**
	 * 是否暂停游戏
	 */
	static ispause: boolean;

	/**
	 * 砍过的木头
	 */
	static barrierCount: number;
	//从config文件中读取数据
	/**
	 * 存放配置文件中读取的障碍物数据
	 */
	static elements: any[] = [];
	/**
	 * 场景的移动速度
	 */
	static speed: number;

	static tree: Tree;

}	