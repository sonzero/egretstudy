/**
 * 角色
 * 
 */
class Player extends GameObject {
	public constructor() {
		super();
		this.init();
		// this.addEventListener( egret.Event.ADDED_TO_STAGE,this.init,this);
	}

	//加速度
	private acceleration: number = 0;

	//显示形态
	public type: BranchType;

	private roleMoveClip: egret.MovieClip = null;
	private mcDataFactory: egret.MovieClipDataFactory

	//计算玩家的宽高时 只计算鸟本身 
	get width() {
		return this.roleMoveClip.width;
	}
	get height() {
		return this.roleMoveClip.height;
	}

	get leftWidth() {
		return this.roleMoveClip.width;
	}

	private init() {
		this.mcDataFactory = new egret.MovieClipDataFactory(RES.getRes("role_man_json"), RES.getRes("role_man_png"));
		this.roleMoveClip = new egret.MovieClip(this.mcDataFactory.generateMovieClipData("left"));
		this.addChild(this.roleMoveClip);
	}

	update(timeStamp: number) {

	}

	public setType(direction: BranchType) {
		//超过一半
		this.type = direction;
		if (direction == BranchType.LEFT) {
			this.roleMoveClip.movieClipData = this.mcDataFactory.generateMovieClipData("left");
			this.roleMoveClip.gotoAndPlay(1, 1);
		} else if (direction == BranchType.RIGHT) {
			this.roleMoveClip.movieClipData = this.mcDataFactory.generateMovieClipData("right");
			this.roleMoveClip.gotoAndPlay(1, 1);
		}
	}
}