/**
 * 树枝
 * 
 */
class BranchItem extends eui.ItemRenderer {

	public left_branch: eui.Image;
	public left_branch_2: eui.Image;
	public body_branch: eui.Image;
	public right_branch: eui.Image;
	public right_branch_2: eui.Image;

	//branch type
	private type: BranchType;

	public constructor() {
		super();
		this.skinName = "resource/game_eui/brick_item.exml";
		console.log("constructor ");
	}

	private createBranch(type: BranchType) {
		if (type == BranchType.BASE) {
			this.body_branch.visible = false;
		} else if (type == BranchType.LEFT) {
			// this.left_branch.visible = true;
			// this.right_branch.visible = false;
			this.showLeftVisible(true);
			this.showRightVisible(false);
			this.body_branch.visible = true;
		} else if (type == BranchType.RIGHT) {
			// this.right_branch.visible = true;
			// this.left_branch.visible = false;
			this.showLeftVisible(false);
			this.showRightVisible(true);
			this.body_branch.visible = true;
		} else {
			// this.left_branch.visible = false;
			// this.right_branch.visible = false;
			this.showLeftVisible(false);
			this.showRightVisible(false);
			this.body_branch.visible = true;
		}

	}

	protected createChildren(): void {
		super.createChildren();
	}

	protected dataChanged(): void {
		//只有add才进来
		console.log("dataChanged " + this.data.value);
		this.type = this.data.value;
		console.log("isLeftShow " + this.left_branch.visible + " isRightShow " + this.right_branch.visible);
		this.createBranch(this.type);
		// this.startAddAnim();
		// if (this.data.isRemove) {
		// 	this.startRemoveAnim();
		// }

	}



	private showLeftVisible(isShow: boolean) {
		this.left_branch.visible = isShow ? true : false;
		this.left_branch_2.visible = isShow ? true : false;;
	}

	private showRightVisible(isShow: boolean) {
		this.right_branch.visible = isShow ? true : false;
		this.right_branch_2.visible = isShow ? true : false;;
	}

	public startRemoveAnim() {
		egret.Tween.get(this.body_branch).to({ x: 350, y: 500, rotation: 45, alpha: 0 }, 200, egret.Ease.sineIn).call(function () {
			
		}
		);
	}

	public startAddAnim() {
		// egret.Tween.get(this.body_branch).to({y: 92 }, 1000, egret.Ease.sineIn).call(function () {

		// }
		// );
		// egret.Tween.get(this.body_branch).to({ x: 0, y: 92 }, 100, egret.Ease.sineIn).call(function () {

		// }
		// );
		// egret.Tween.get(this.body_branch).to({ x: 0, y: 92 }, 100, egret.Ease.sineIn).call(function () {

		// }
		// );
	}

}