/**
 * 木头
 * 
 */
class Tree extends GameObject {

	public constructor(type: BranchType) {
		super();
		this.init();
		// this.addEventListener( egret.Event.ADDED_TO_STAGE,this.init,this);
		this.type = type;
	}
	//木头图片
	private bodyBranch: egret.Bitmap;
	//left树枝图片
	private leftBranch: egret.Bitmap;
	//right树枝图片
	private rightBranch: egret.Bitmap;
	//branch type
	private type: BranchType;

	private init() {
		this.bodyBranch = GameUtils.createBitmapByName("body_jpg");
		this.bodyBranch.visible = true;
		this.addChild(this.bodyBranch);
		this.leftBranch = GameUtils.createBitmapByName("left_branch_png");
		this.leftBranch.x = this.bodyBranch.x - this.leftBranch.width;
		this.leftBranch.visible = false;
		this.addChild(this.leftBranch);
		this.rightBranch = GameUtils.createBitmapByName("right_branch_png");
		this.rightBranch.x = this.bodyBranch.x + this.rightBranch.width;
		this.rightBranch.visible = false;
		this.addChild(this.rightBranch);
	}

	public createBranch() {
		if (this.type == BranchType.BASE) {

		} else if (this.type == BranchType.LEFT) {
			this.leftBranch.visible = true;
			this.rightBranch.visible = false;
		} else if (this.type == BranchType.RIGHT) {
			this.rightBranch.visible = true;
			this.leftBranch.visible = false;
		} else {
			this.leftBranch.visible = false;
			this.rightBranch.visible = false;
		}
	}


}