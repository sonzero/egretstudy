
/**
 * 数据管理类
 * 
 */
class GameManager {

	//分数
	public score: number = 0;
	private static mInstance: GameManager;
	public static isAlive: boolean = false;
	private updateListeners = {};
	private gamelistener: GameListener;
	public static getInstance() {
		if (!GameManager.mInstance) {
			GameManager.mInstance = new GameManager();
		}
		return GameManager.mInstance;
	}

	public constructor() {
		MessageCenter.getInstance();
		this.score = 0;
	}

	public startGame() {
		GameManager.isAlive = true;
		this.score = 0;
		this.gamelistener.startGame();
		SoundManager.getInstance().playBgMusic();
	}

	public stopGame() {
		GameManager.isAlive = false;
		this.gamelistener.stopGame(false);
		SoundManager.getInstance().playWrong();
		SoundManager.getInstance().stopBgMusic();
	}

	public getScore(): number {
		return this.score;
	}

	public updateScore() {
		if (GameManager.isAlive) {
			this.score++;
		}
	}


	public resetScore() {
		GameManager.isAlive = true;
		this.score = 0;
	}


	//分享视频后恢复
	public restartGame(isRestart: boolean) {
		GameManager.isAlive = true;
		if (!isRestart) {
			this.score = 0;
		}
		this.gamelistener.restartGame();
		SoundManager.getInstance().playBgMusic();
	}

	//分享视频后恢复
	public backHome() {
		GameManager.isAlive = false;
		this.score = 0;
		this.gamelistener.stopGame(true);
	}

	/**
	* 注册从GameView的Update消息
	* @param key 消息标识
	* @param callbackFunc 处理函数
	* @param callbackObj 处理函数所属对象
	*/
	public registerOnUpdateViewListener(key: any, callbackFunc: Function, callbackObj: any) {
		MessageCenter.getInstance().addListener(key, callbackFunc, callbackObj);
	}

	/**
     * 移除GameView的Update消息
     * @param key 消息标识
     * @param callbackFunc 处理函数
     * @param callbackObj 处理函数所属对象
     */
	public removeServerMsg(key: any, callbackFunc: Function, callbackObj: any): void {
		MessageCenter.getInstance().removeListener(key, callbackFunc, callbackObj);
	}

	public registerGameListener(listener: GameListener) {
		this.gamelistener = listener;
	}

	public removeGameListener(listener: GameListener) {
		this.gamelistener = null;
	}
	

}	