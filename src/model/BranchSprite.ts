class BranchSprite extends GameObject {

	public static WIDTH: number = 100;
	public static HEIGHT: number = 92;
	//图片
	private brickBitmap: egret.Bitmap;
	//branch type
	public type: BranchType;

	public constructor(type: BranchType) {
		super();
		this.type = type;
		this.init();
		// this.addEventListener( egret.Event.ADDED_TO_STAGE,this.init,this);
		this.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.destoryView, this);
	}

	private init() {
		this.brickBitmap = GameUtils.createBitmapByName("brick_png");
		this.brickBitmap.width = BranchSprite.WIDTH;
		this.brickBitmap.height = BranchSprite.HEIGHT;
		this.addChild(this.brickBitmap);
	}

	private destoryView() {
		// console.log("destoryView:");
		// this.dispose();
	}

	public startShowCartoon(gamePlayerType: BranchType): void {
		egret.Tween.removeTweens(this);
		this.y -= 0;
		this.alpha = 0;
		if (gamePlayerType == BranchType.LEFT) {
			egret.Tween.get(this).to({ x: this.x + 200, y: this.y + 100, alpha: 1, rotation: 45 }, 200).call(this.dispose, this);
		} else {
			egret.Tween.get(this).to({ x: this.x - 200, y: this.y + 100, alpha: 1, rotation: -45 }, 200).call(this.dispose, this);
		}
		// egret.Tween.get(this).to({ x: this.x + 200, y: this.y + 100, alpha: 1 }, 200);
		// this.dispose();
	}

	public startMoveCartoon(): void {
		egret.Tween.removeTweens(this);
		this.y -= BranchSprite.HEIGHT;
		this.alpha = 1;
		egret.Tween.get(this).to({ y: this.y + BranchSprite.HEIGHT, alpha: 1 }, 200).call(function () {
			// console.log("startMoveCartoon:");
		}, this);
	}


	protected dispose(): void {
		// console.log("dispose:");
		egret.Tween.removeTweens(this);
		if (this.parent != null) {
			this.parent.removeChild(this);
		}
		this.brickBitmap = null
	}

	public destory() {
		this.dispose();
	}

	public setType(branchType: BranchType) {
		this.type = branchType;
	}





}	