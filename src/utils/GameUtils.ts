class GameUtils {
    public constructor() {
    }

    /**
    * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
    * 全局函数 
    */
    public static createBitmapByName(name: string) {
        let result = new egret.Bitmap();
        let texture: egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }


    //获取小数点后面的2位数,这个方法有点粗暴后面可以看下有什么有更好的方法获取小数点两位。
    public static getNumberPoint(num: number, pointNumber: number): number {
        var number = num.toFixed(pointNumber);
        return Number("0." + number.split(".")[1]);
    }

}