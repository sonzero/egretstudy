class WeixinUtils {

    public constructor() {
    }

    public static saveWxScore(score: string) {
        // console.log(" saveWxScore " + score);
        platform.setUserCloudStorage([{ key: "score", value: score }]);
    }

    public static shareWx() {
        var desc: string = this.getShareTips();
        var imgurl: string = "resource/assets/" + "share" + ".jpg";
        platform.shareAppMessage(desc + "疯狂拆迁队，快来爽一把", imgurl, desc);
    }

    public static showWxRank() {

    }

    private static clickGroup() {
        var desc: string = "疯狂伐木工";
        var imgurl: string = "resource/assets/icon" + (1 + Math.floor(Math.random() * 4)) + ".jpg";

        return new Promise((resolve, reject) => {
            platform.updateShareMenu(true).then(data => {
                console.log("updateShareMenu: ", data);
                if (data) {
                    return platform.shareApp("群主别踢,我就是看看谁的手速最快," + desc, imgurl, desc).then(data => {
                        if (data && data.shareTickets && data.shareTickets.length > 0) {
                            this.groupRank(data.shareTickets[0]);
                            resolve(true);
                        } else {
                            resolve(false);
                        }
                    });
                } else {
                    resolve(false);
                }
            })
        });
    }

    private static groupRank(shareTicket): void {
        platform.sendShareData({ command: "open", type: "group", groupid: shareTicket });
    }

    public static getShareTips(): string {
        var score: number = GameManager.getInstance().getScore();
        if (0 == score) {
            return "";
        }
        else {
            return "我已经拆了:" + score + "块砖啦，你也一起来啊!";
        }
    }

}