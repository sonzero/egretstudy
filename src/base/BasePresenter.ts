abstract class BasePresenter<V extends BaseView>{

	private view: V;
	
	//绑定
	public attachView(view: V) {
		this.view = view;
	}

	//解绑
	public detachView() {
		this.view = null;
	}

	public getView(): V {
		return this.view;
	}

}