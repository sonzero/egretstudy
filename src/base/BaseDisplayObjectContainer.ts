abstract class BaseDisplayObjectContainer<P extends BasePresenter<V>, V extends BaseView> extends egret.DisplayObjectContainer{
	//fix me暂时没完善
	private presenter: P;
	private view: V;

	public abstract createPresenter(): P;

	public abstract createView(): V;


	// public constructor() {
	// 	super();
	// 	this.addEventListener(egret.Event.ADDED_TO_STAGE,this.onCreateView,this);
	// 	//场景删除时候的回调，暂时理解类似ondestoryView
	// 	this.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.destoryView, this);
	// }

	// //添加皮肤的时候自动调用该函数
	// protected partAdded(partName: string, instance: any): void {
	// 	super.partAdded(partName, instance);
	// }

	// // 组件加载完毕之后调用该函数,相当于是onCreate
	// protected childrenCreated(): void {
	// 	super.childrenCreated();
	// 	if (this.presenter == null) {
	// 		this.presenter = this.createPresenter();
	// 	}
	// 	if (this.view == null) {
	// 		this.view = this.createView();
	// 	}
	// 	//绑定
	// 	if (this.presenter != null && this.view != null) {
	// 		this.presenter.attachView(this.view);
	// 	}
	// }

	// private destoryView() {
	// 	//解绑
	// 	if (this.presenter != null) {
	// 		this.presenter.detachView();
	// 	}
	// }


}